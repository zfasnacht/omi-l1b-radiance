from netCDF4 import Dataset 
from pylab import *
from scipy.interpolate import interp1d

#Reading Radiances from OMI L1b file 
filename = 'OMI-Aura_L1-OML1BRVG_2005m0321t0122-o003623_v0400-2021m0513t1538.nc'

f = Dataset(filename,'r')
radiance = f['/BAND3_RADIANCE/STANDARD_MODE/OBSERVATIONS/radiance'][0,:,:,:]
radiance_wavelengths = f['/BAND3_RADIANCE/STANDARD_MODE/INSTRUMENT/wavelength'][0,:,:]
longitude = f['/BAND3_RADIANCE/STANDARD_MODE/GEODATA/longitude'][0,:,:]
latitude = f['/BAND3_RADIANCE/STANDARD_MODE/GEODATA/latitude'][0,:,:]

#NetCDF4 makes masked arrays where values are NaN, here we swap out the mask for NaN
radiance = np.ma.filled(radiance,float('nan'))
radiance_wavelengths = np.ma.filled(radiance_wavelengths,float('nan'))


#Reading irradiances 
filename = 'OMI-Aura_L1-OML1BIRR_2005m0321t0937-o003628_v0400-2021m0513t1538.nc'
f = Dataset(filename,'r')
irradiance = f['/BAND3_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/irradiance'][0,0,:,:]
irradiance_wavelengths = f['/BAND3_IRRADIANCE/STANDARD_MODE/INSTRUMENT/wavelength'][0,:,:]

#NetCDF4 makes masked arrays where values are NaN, here we swap out the mask for NaN
irradiance = np.ma.filled(irradiance,float('nan'))
irradiance_wavelengths = np.ma.filled(irradiance_wavelengths,float('nan'))

#Creating a new array to store irradiances that are interpolated to radiance wavelengths 
#Zeros_like simply makes an array filled with 0's that has the same size as the provided array 
irradiance_interpolated = np.zeros_like(irradiance) + np.nan

#Looping through the rows and interpolating the irradiances to the radiance wavelengths 
for row in range(60):
    linear_interp = interp1d(irradiance_wavelengths[row,:],irradiance[row,:],kind='linear',bounds_error=False,fill_value=np.nan)
    irradiance_interpolated[row,:] = linear_interp(radiance_wavelengths[row,:])

#Plotting radiances, irradiances, and sun-normalizaed radiances for sample pixel (scanline 300, row 30)
fig, axes = plt.subplots(3,figsize=(13,10))
axes[0].plot(radiance_wavelengths[30,:],radiance[300,30,:],lw=2,color='k')
axes[1].plot(radiance_wavelengths[30,:],irradiance_interpolated[30,:],lw=2,color='k')
axes[2].plot(radiance_wavelengths[30,:],radiance[300,30,:]/irradiance_interpolated[30,:],lw=2,color='k')

for ax in axes.flatten():
    ax.set_xlim(430,470)

axes[0].set_ylabel('Radiance',fontsize=15)
axes[1].set_ylabel('Irradiance',fontsize=15)
axes[2].set_ylabel('Sun Normalized Radiance',fontsize=15)

axes[2].set_ylim(0.075,0.08)

#plt.show() will open an interactive window to view the plot, plt.savefig() will save the image
plt.show()
stop
plt.savefig('OMI_SunNormalizedRadiances.png')
